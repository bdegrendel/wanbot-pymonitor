#!/usr/bin/env python3

import json
import redis
from tkinter import *

class Goal:
  def __init__(self, root, type_, id_):
    self._root = root
    self._type = type_
    self._id = id_
    self._what = self._created = self._achieved = self._duration = self._incubate = None
    self._actions = []

    self._label = Label(self._root, text="")

  def get_id(self):
    return self._id

  def update(self, payload):
    self._what = payload['what']
    self._created = payload['created']['why']

    if 'achieved' in payload:
      self._achieved = payload['achieved']['why']
    else:
      self._achieved = None

    if 'duration' in payload:
      self._duration = payload['duration']
    else:
      self._duration = None

    if 'incubate' in payload:
      self._incubate = payload['incubate']
    else:
      self._incubate = None

    label = """Goal#{id} {type} -- {what}
    {created}""".format(**{'id': self._id, 'type': self._type, 'what': self._what, 'created': self._created})

    if self._achieved:
      label += "; Achieved: {}".format(self._achieved)

    label += "\n"
    if self._incubate:
      label += "Incubate: {}s\n".format(self._incubate)

    if self._duration:
      label += "Duration: {}s\n".format(self._duration)

    label += "\n"
    if len(payload['actions']) == 0:
      label += "(No actions performed)\n"
    else:
      for k, v in payload['actions'].items():
        label += "Action {} -- {}".format(k, v['what'])
        if 'done' in v:
          label += "; complete"
        label += "\n"

    label += "\n"

    if self._achieved:
      self._label.config(bg='deep sky blue')
    elif 'active' in payload:
      self._label.config(bg='green')
    else:
      self._label.config(bg='red')
    self._label.config(text=label)
    self._label.pack(side=TOP)

  def drop(self):
    self._label.destroy()

class Monitor:
  def __init__(self, root, redis):
    self._root = root
    self._redis = redis

    self._root.bind("<Destroy>", self.destroy)

    self._pubsub_thread = None

    self._goals = {}

  def start(self):
    p = self._redis.pubsub(ignore_subscribe_messages=True)
    p.subscribe(**{'wanbot-agent-goal-status': self.status_recieved})
    self._pubsub_thread = p.run_in_thread(sleep_time=0.005)

  def status_recieved(self, message):
    data = json.loads(message['data'].decode("utf-8"))

    self._root.after_idle(self.update, data)

  def update(self, data):
    if type(data) != list:
      print("WARN: Recieved status update that isn't a list:", data)
      return
    seen = set()
    for goal in data:
      if not 'type' in goal:
        print("WARN: Recieved status update with goal that lacks a type", goal)
        next
      goal_type = goal['type']
      if not 'data' in goal or type(goal['data']) != dict:
        print("WARN: Recieved status update with a {} goal that lacks a dictionary payload".format(goal_type), goal)
        next
      payload = goal['data']
      if not 'id' in payload:
        print("WARN: Recieved status update with a {} goal that lacks an id".format(goal_type), goal)
        next
      goal_id = payload['id']
      seen.add(goal_id)
      try:
        if not goal_id in self._goals:
          self._goals[goal_id] = Goal(self._root, goal_type, goal_id)

        self._goals[goal_id].update(payload)
      except KeyError as e:
        print("WARN: Unable to get key out of {} goal#{}:".format(goal_type, goal_id), e)
    to_remove = []
    for goal_id in self._goals:
      if not goal_id in seen:
        to_remove.append(goal_id)

    for goal_id in to_remove:
      self._goals[goal_id].drop()
      del self._goals[goal_id]


  def destroy(self, event):
    if event.widget == self._root:
      if self._pubsub_thread:
        self._pubsub_thread.stop()
        self._pubsub_thread = None

def main():
  root = Tk()
  root.minsize(200, 200)
  root.title("WanBot Goal Monitor")

  # TODO: Get hostname/port from command line arguments
  r = redis.StrictRedis(host='localhost', port=6379, db=0)

  monitor = Monitor(root, r)
  monitor.start()
  root.mainloop()

if __name__ == "__main__": main()

